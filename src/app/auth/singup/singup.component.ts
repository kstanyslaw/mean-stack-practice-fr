import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.scss']
})
export class SingupComponent implements OnInit, OnDestroy {
  
  isLoading = false;
  private authStatusSub: Subscription;
  
  constructor(public authService: AuthService) { }
  
  onSingup(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.createUser(form.value.email, form.value.password);
  }
  
  ngOnInit() {
    this.authStatusSub = this.authService.getAuthStatusListener().subscribe(
      AuthStatus => {
        this.isLoading = false;
      }
    );
  }
  
  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
  }
}
